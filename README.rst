erlSM - erlang Security Module
==============================

Prova de conceito da viabilidade da criação de um Security Module implementado em erlang
provendo as seguintes funcionalidades:

- Gateway HTTPS
- Assinatura de XMLs (não implementado)

O objetivo deste projeto é prover um método conveniente e seguro para a gerência de certificados
e chaves privadas utilizadas por outras aplicações. Se utilizado corretamente este projeto provê
um nível de proteção igual ou superior ao FIPS 140-2, de forma muito mais conveniente.


Dependências, compilação e execução
-----------------------------------

Este projeto utiliza o rebar para gerenciamento de dependencias e o relx para o gerenciamento de
releases. Ambos precisam estar instalados nas máquinas de desenvolvimento.
Este código foi testado na versão R16B03-1.


Compilação
^^^^^^^^^^

.. code-block:: bash

    # Obter as dependências do projeto
    rebar get-deps

    # Compilar código atual
    rebar compile

    # Gerar uma release
    relx

    # Executar o projeto
    ./_rel/bin/erlsm_server console 
