-module(notacarioca_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

-record(environment, {
    name :: atom(),
    host :: binary(),
    url :: binary(),
    gw_url :: binary()}).

init(_type, Req, _Opts) ->
    {ok, Req, []}.

handle(Req, State) ->
    % Read all the data from the request
    {Method, Req2} = cowboy_req:method(Req),
    {Url, Req3} = cowboy_req:url(Req2),
    {Headers, Req4} = cowboy_req:headers(Req3),
    {ok, Body, Req5} = cowboy_req:body(Req4),
    {Environment, Req6} = create_environment(Req5),

    {ok, {<<"basic">>, {User, Password}}, Req7} = cowboy_req:parse_header(<<"authorization">>, Req6),
    {ok, SUrl, SHeaders, Body} = translate_request(Url, Headers, Body, Environment),

    % Proxy the request
    Opts = [{pool, default}, {recv_timeout, 15000}, {ssl_options, 
        [{versions, [tlsv1, sslv3]}] ++ get_user_certificate(User, Password)
    }],
    Req8 = case make_request(Method, SUrl, SHeaders, Body, Opts) of
        {ok, RespStatus, RespHeaders, ClientRef} ->
            send_response(ClientRef, Req7, {RespStatus, RespHeaders}, Environment, Opts);
        {error, {keyfile, {badmatch, {error, {asn1, {invalid_length, _Len}}}}}} ->
            % Wrong password
            cowboy_req:reply(403, Req7),
            Req7;
        {error, {options, {keyfile, _Filename, {error, enoent}}}} ->
            % Unknown user
            cowboy_req:reply(403, Req7),
            Req7;
        {error, Reason} ->
            io:format("Error from make_request: ~p~n", [Reason]),
            Req7;
        Else ->
            io:format("Unexpected make_request message: ~p~n", [Else]),
            Req7
    end,
    {ok, Req8, State}.

terminate(_Reason, _Req, _State) ->
    ok.

%%
% Internal functions
create_environment(Req) ->
    case cowboy_req:binding('environment', Req) of
        {<<"homologacao">>, ReqX} ->
            EnvRecord = #environment{
                name = sandbox,
                host = <<"homologacao.notacarioca.rio.gov.br">>,
                url = <<"https://homologacao.notacarioca.rio.gov.br">>,
                gw_url = <<"http://127.0.0.1:8080/homologacao/notacarioca">>},
            {EnvRecord, ReqX};
        {<<"producao">>, ReqX} ->
            EnvRecord = #environment{
                name = production,
                host = <<"notacarioca.rio.gov.br">>,
                url = <<"https://notacarioca.rio.gov.br">>,
                gw_url = <<"http://127.0.0.1:8080/producao/notacarioca">>},
            {EnvRecord, ReqX}
    end.

get_user_certificate(User, Password) ->
    {ok, KeysStorage} = application:get_env(erlsm, keys_storage),
    CertFile = KeysStorage ++ binary:bin_to_list(User) ++ "/certificate",
    KeyFile = KeysStorage ++ binary:bin_to_list(User) ++ "/private_key",
    [{certfile, CertFile}, {keyfile, KeyFile}, {password, erlang:binary_to_list(Password)}].

translate_request(Url, Headers, Body, Env) ->
    SUrl = translate_to_server(Url, Env),
    SHeaders = lists:keyreplace(<<"host">>, 1, Headers, {<<"host">>, Env#environment.host}),
    {ok, SUrl, SHeaders, Body}.
translate_response(Headers, Body, Env) ->
    CBody = case lists:keysearch(<<"Content-Encoding">>, 1, Headers) of
        {value, {<<"Content-Encoding">>, <<"gzip">>}} ->
            zlib:gzip(translate_to_client(zlib:gunzip(Body), Env));
        _ ->
            translate_to_client(Body, Env)
    end,
    {ok, Headers, CBody}.

translate_to_server(Subject, Env) ->
    binary:replace(Subject,
        Env#environment.gw_url,
        Env#environment.url,
        [global]).
translate_to_client(Subject, Env) ->
    binary:replace(Subject,
        Env#environment.url,
        Env#environment.gw_url,
        [global]).

make_request(Method, Url, Headers, Body, Opts) when is_binary(Url) ->
    UpdatedUrl = binary:bin_to_list(Url),
    UpdatedHeaders = normalize_headers(Headers),
    make_request(Method, UpdatedUrl, UpdatedHeaders, Body, Opts);
make_request(<<"GET">>, Url, Headers, Body, Opts) ->
    hackney:request(get, Url, Headers, Body, Opts);
make_request(<<"POST">>, Url, Headers, Body, Opts) ->
    hackney:request(post, Url, Headers, Body, Opts);
make_request(<<"OPTIONS">>, Url, Headers, Body, Opts) ->
    hackney:request(options, Url, Headers, Body, Opts).

normalize_headers(Headers) ->
    lists:keydelete(<<"authorization">>, 1, Headers).

send_response(ClientRef, Request, Response, Env, _Opts) ->
    {RespStatus, RespHeaders} = Response,
    RespBody = case hackney:body(ClientRef) of
        {ok, Content} ->
            Content;
        {error, {closed, Content}} ->
            Content
    end,

    io:format("response status is ~tp ~n, ", [RespStatus]),

    {ok, RespHeaders, CBody} = translate_response(RespHeaders, RespBody, Env),
    cowboy_req:reply(RespStatus, RespHeaders, CBody, Request).
